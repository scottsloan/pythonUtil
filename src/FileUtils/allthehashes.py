import subprocess
import os
import getopt, sys

hashTools = {
    'md5' : 'md5sum',
    'sha1' : 'sha1sum',
    'sha256' : 'sha256sum',
    'sha244' : 'sha224sum', 
    'sha384' : 'sha384sum',
    'sha512' : 'sha512sum'
}

def isExe(fPath):
    return os.path.isfile(fPath) and os.access(fPath, os.X_OK)

def which(program):
    fpath, fname = os.path.split(program)

    if fpath:
        if isExe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exeFile = os.path.join(path, program)

            if isExe(exeFile):
                return exeFile

    return None

def getTheHashes(file):
    for key,tool in hashTools.items():
        if which(tool) != None:
            process = subprocess.run([tool, file], check=True, stdout=subprocess.PIPE, universal_newlines=True)
            print('[+] '+ key + ' ' + process.stdout)
        else:
            print('[-] '+ key + ' is not installed')

def usage():
    print("Computes the hash of the input file.")

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:", ["help", "file="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2);

    filePath = None

    for o, a in opts:
        if o in ("-f", "--file"):
            filePath = a
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    getTheHashes(filePath)

if __name__ == '__main__':
    main()
