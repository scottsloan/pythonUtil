import io
import time
import getopt, sys

def usage():
    print "follows the tail of a file"

def tail(file):
    file.seek(0,2)

    while True:
        line = file.readline();

        if not line:
            time.sleep(0.1)
            continue
        
        yield line

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:", ["help", "file="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2);

    filePath = None

    for o, a in opts:
        if o in ("-f", "--file"):
            filePath = a
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    logfile = open(filePath,"r", buffering = 1)
    loglines = tail(logfile)
    for line in loglines:
        print line,

if __name__ == '__main__':
    main()
