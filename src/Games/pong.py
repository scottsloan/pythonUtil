# Import required libraray 
import turtle 

blockSize = 20
chaosFactor = .75

player1Name = "Scott"
player1Score = 0

player2Name = "Calvin"
player2Score = 0

# Create GameScreen 
GameScreen = turtle.Screen() 
GameScreen.title("Pong game") 
GameScreen.setup(width=1024, height=768) 

# Ball of circle shape 
ball = turtle.Turtle() 
ball.speed(0)          
ball.shape("circle") 
ball.color("red") 
ball.penup() 
ball.setposition(0, 0) 
ball.dx = 3
ball.dy = -3

# Left paddle 
leftPaddle = turtle.Turtle() 
leftPaddle.speed(0) 
leftPaddle.shape("square") 
leftPaddle.color("black") 
leftPaddle.shapesize(stretch_wid=6, stretch_len=1) 
leftPaddle.penup() 
leftPaddle.setposition(-400, 0) 

# Right paddle 
rightPaddle = turtle.Turtle() 
rightPaddle.speed(0) 
rightPaddle.shape("square") 
rightPaddle.color("black") 
rightPaddle.shapesize(stretch_wid=6, stretch_len=1) 
rightPaddle.penup() 
rightPaddle.setposition(400, 0) 

# Displays the score 
scoreBoard = turtle.Turtle() 
scoreBoard.speed(0) 
scoreBoard.color("blue") 
scoreBoard.penup() 
scoreBoard.hideturtle() 
scoreBoard.setposition(0, 325) 

# Functions to move paddle vertically 
def leftPaddle_up(): 
    y = leftPaddle.ycor() + blockSize
    
    if(abs(y) > GameScreen.window_height() / 2):
        y = leftPaddle.ycor() 

    leftPaddle.sety(y) 

def leftPaddle_down(): 
    y = leftPaddle.ycor() - blockSize
    
    if(abs(y) > GameScreen.window_height() / 2):
        y = leftPaddle.ycor()

    leftPaddle.sety(y) 

def rightPaddle_up(): 
    y = rightPaddle.ycor() + blockSize

    if(abs(y) > GameScreen.window_height() / 2):
        y = rightPaddle.ycor() 

    rightPaddle.sety(y) 

def rightPaddle_down(): 
    y = rightPaddle.ycor() - blockSize

    if(abs(y) > GameScreen.window_height() / 2):
        y = rightPaddle.ycor() 

    rightPaddle.sety(y) 

# Keyboard bindings 
GameScreen.listen() 
GameScreen.onkeypress(leftPaddle_up, "w") 
GameScreen.onkeypress(leftPaddle_down, "s") 
GameScreen.onkeypress(rightPaddle_up, "Up") 
GameScreen.onkeypress(rightPaddle_down, "Down") 

def calculateNewPosition():
    newX = ball.xcor() + ball.dx 
    newY = ball.ycor() + ball.dy + chaosFactor

    #check for collsion with top and bottom of screen
    if(abs(newY) >= GameScreen.window_height() / 2):
        newY = ball.ycor()
        ball.dy = ball.dy * -1

    #check for collsion with right paddle
    edgeOfPaddle = rightPaddle.xcor() - blockSize
    if(abs(newX) - abs(edgeOfPaddle) > 0 and abs(newX - edgeOfPaddle) < blockSize):
        topPady = rightPaddle.ycor() + (3 * blockSize)
        bottomPady = rightPaddle.ycor() - (3 * blockSize)

        if(newY <= topPady and newY >= bottomPady):
            newX = edgeOfPaddle
            ball.dx = ball.dx * -1

            # change the direction of the ball based on where we hit on the paddle
            if(newY > rightPaddle.ycor()):
                ball.dy = abs(ball.dy)

            if(newY < rightPaddle.ycor()):
                ball.dy = abs(ball.dy) * -1

    #check for collsion with left paddle
    edgeOfPaddle = leftPaddle.xcor() + blockSize

    if(abs(newX) - abs(edgeOfPaddle) > 0 and abs(newX  - edgeOfPaddle) < blockSize):
        topPady = leftPaddle.ycor() + (3 * blockSize)
        bottomPady = leftPaddle.ycor() - (3 * blockSize)

        if(newY <= topPady and newY >= bottomPady):
            newX = edgeOfPaddle
            ball.dx = ball.dx * -1

            # change the direction of the ball based on where we hit on the paddle
            if(newY > leftPaddle.ycor()):
                ball.dy = abs(ball.dy)

            if(newY < leftPaddle.ycor()):
                ball.dy = abs(ball.dy) * -1

    ball.setposition(newX, newY) 
    
def resetBall():
    if(leftPaddle.ycor() % 3 == 0 ):
        ball.dx = ball.dx * -1

    if(rightPaddle.ycor() %3 == 0 ):
        ball.dy = ball.dy * -1 

    ball.home()

def checkForPoint():
    global player1Score
    global player2Score

    if(ball.xcor() - rightPaddle.xcor() >= blockSize):
        player1Score += 1
        updateScoreboard()
        resetBall()

    if(abs(ball.xcor()) - abs(leftPaddle.xcor()) >= blockSize):
        player2Score += 1
        updateScoreboard()
        resetBall()

def updateScoreboard():
    scoreBoard.clear() 
    scoreBoard.write( "{} : {} vs. {} : {}".format(player1Name, player1Score, player2Name, player2Score), align="center", font=("Courier", 24, "normal")) 

updateScoreboard()

while(True):
    # Update the GameScreen 
    GameScreen.update() 

    # Calculate new state
    calculateNewPosition()
    checkForPoint()
